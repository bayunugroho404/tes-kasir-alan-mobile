/**
 * Created by rumah coding dev ( Bayu Nugroho)
 * Copyright (c) 2020 . All rights reserved.
 */

class ItemProduct{
  String name;
  int qty;
  int harga;
  bool isListed = false;
  String img;
  int id;

  ItemProduct({this.name, this.qty, this.harga,this.img,this.isListed,this.id});
}