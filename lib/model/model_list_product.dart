/**
 * Created by rumah coding dev ( Bayu Nugroho)
 * Copyright (c) 2020 . All rights reserved.
 */

class ModelListProduct{
  String name;
  int qty;
  int harga;
  String img;
  int id;

  ModelListProduct({this.name, this.qty, this.harga, this.img,this.id});
}