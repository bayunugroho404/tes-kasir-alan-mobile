import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

/**
 * Created by rumah coding dev ( Bayu Nugroho)
 * Copyright (c) 2020 . All rights reserved.
 */

void showToast(BuildContext context,String msg, {int duration, int gravity}) {
  Toast.show(msg, context, duration: Toast.LENGTH_SHORT);
}