// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:indonesia/indonesia.dart';
// import 'package:pos_bayu/model/model_list_product.dart';
//
// /**
//  * Created by rumah coding dev ( Bayu Nugroho)
//  * Copyright (c) 2020 . All rights reserved.
//  */
//
// void bottomSheetProduct(double screenHight,double screenWidth, List<ModelListProduct> listProductcard){
//   Get.bottomSheet(
//     Container(
//       color: Colors.grey[700],
//       child: Container(
//         height: screenHight / 1.3,
//         child: ListView.builder(
//           physics: ClampingScrollPhysics(),
//           itemCount: listProductcard.length,
//           scrollDirection: Axis.vertical,
//           itemBuilder: (context, index) {
//             return Container(
//                 child: InkWell(
//                   onTap: () {
//                     // onClickProduct(listProduct[index]);
//                     // print(listProductcard.length.toString() + " dhsajl");
//                   },
//                   child: Card(
//                       elevation: 4,
//                       child: Column(
//                         children: <Widget>[
//                           Row(
//                             children: [
//                               Flexible(
//                                 flex:1,
//                                 child: Container(
//                                   child: Image.asset(
//                                     listProductcard[index].img,
//                                     fit: BoxFit.cover,
//                                     width: screenWidth /4,
//                                     height: 100,
//                                   ),
//                                 ),
//                               ),
//                               Flexible(
//                                 flex: 1,
//                                 child: Container(
//                                   child: Column(
//                                     mainAxisAlignment: MainAxisAlignment.start,
//                                     crossAxisAlignment: CrossAxisAlignment.start,
//                                     children: [
//                                       Container(
//                                         margin: EdgeInsets.only(left: 4),
//                                         child: Text(
//                                             "${listProductcard[index].name}",
//                                             textAlign: TextAlign.left,
//                                             style: GoogleFonts.kodchasan()
//                                                 .copyWith(
//                                                 fontWeight:
//                                                 FontWeight.bold,
//                                                 color: Colors.black,
//                                                 fontSize: 14)),
//                                       ),
//                                       SizedBox(
//                                         height: 4,
//                                       ),
//                                       Row(
//                                         children: [
//                                           Container(
//                                             margin: EdgeInsets.only(left: 4),
//
//                                             child: Text(
//                                                 "${rupiah(listProductcard[index].harga)}",
//                                                 textAlign: TextAlign.left,
//                                                 style: GoogleFonts
//                                                     .kodchasan()
//                                                     .copyWith(
//                                                     color:
//                                                     Colors.blue,
//                                                     fontWeight:
//                                                     FontWeight
//                                                         .bold,
//                                                     fontSize: 10)),
//                                           ),
//                                           Container(
//                                             margin:
//                                             EdgeInsets.only(left: 0),
//                                             child: Text("/ porsi",
//                                                 textAlign: TextAlign.left,
//                                                 style: GoogleFonts
//                                                     .kodchasan()
//                                                     .copyWith(
//                                                     color:
//                                                     Colors.black,
//                                                     fontWeight:
//                                                     FontWeight
//                                                         .bold,
//                                                     fontSize: 10)),
//                                           ),
//                                         ],
//                                       ),
//                                     ],
//                                   ),
//                                 ),
//                               ),
//                               Flexible(
//                                 flex:1,
//                                 child: Row(
//                                   children: [
//                                     Container(
//                                       width: 30,
//                                       decoration: BoxDecoration(
//                                         color: Colors.black,
//                                         shape: BoxShape.circle,
//                                       ),
//                                       child: Center(
//                                         child: IconButton(
//                                           onPressed: () {},
//                                           icon: Icon(
//                                             Icons.remove,
//                                             color: Colors.white,
//                                             size: 16,
//                                           ),
//                                         ),
//                                       ),
//                                     ),
//
//                                     SizedBox(width: 8,),
//                                     Text('${listProductcard[index].qty}'),
//                                     SizedBox(width: 8,),
//                                     Container(
//                                       width: 30,
//                                       decoration: BoxDecoration(
//                                         color: Colors.black,
//                                         shape: BoxShape.circle,
//                                       ),
//                                       child: Center(
//                                         child: IconButton(
//                                           onPressed: () {
//                                             addQuantity(listProductcard[index]);
//                                           },
//                                           icon: Icon(
//                                             Icons.add,
//                                             color: Colors.white,
//                                             size: 16,
//                                           ),
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               )
//                             ],
//                           ),
//                           SizedBox(
//                             height: 4,
//                           ),
//
//                           SizedBox(
//                             height: 4,
//                           ),
//                         ],
//                       )),
//                 ));
//           },
//         ),
//       ),
//     ),
//   );
// }
//
// void addQuantity(ModelListProduct listProductcard) {
//
// }