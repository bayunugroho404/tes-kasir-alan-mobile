import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:indonesia/indonesia.dart';
import 'package:pos_bayu/model/model_item_product.dart';
import 'package:pos_bayu/model/model_list_product.dart';
import 'package:pos_bayu/ui/widget/bottom_sheet.dart';
import 'file:///C:/Users/baay4/AndroidStudioProjects/pos_bayu/lib/ui/view/print.dart';
import 'package:pos_bayu/ui/widget/tile.dart';
import 'package:pos_bayu/ui/widget/toast.dart';
import 'package:pos_bayu/utils/size_config.dart';

/**
 * Created by rumah coding dev ( Bayu Nugroho)
 * Copyright (c) 2020 . All rights reserved.
 */

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<ItemProduct> listProduct = [];
  TextEditingController uangController = TextEditingController();
  TextEditingController totalHargaController = TextEditingController();
  TextEditingController kembalianController = TextEditingController();
  List<ModelListProduct> listProductcard = [];
  int totalHarga = 0;
  int totalQty = 1;

  @override
  void initState() {
    addProduct();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('POS')),
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              height: SizeConfig.screenHight / 1.3,
              child: GridView.builder(
                physics: ScrollPhysics(),
                itemCount: listProduct.length,
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Container(
                      width: SizeConfig.screenWidth / 2,
                      child: InkWell(
                        onTap: () {
                          if (listProductcard.length > 7) {
                            showToast(context,
                                "gagal, product terlalu banyak didalam keranjang");
                          } else {
                            onClickProduct(listProduct[index]);
                            totalHargas();
                          }
                        },
                        child: Card(
                            elevation: 4,
                            child: Column(
                              children: <Widget>[
                                Image.asset(
                                  listProduct[index].img,
                                  fit: BoxFit.cover,
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Row(
                                  children: [
                                    Flexible(
                                      flex: 3,
                                      child: Container(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 8),
                                              child: Text(
                                                  "${listProduct[index].name}",
                                                  textAlign: TextAlign.left,
                                                  style: GoogleFonts.kodchasan()
                                                      .copyWith(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.black,
                                                          fontSize: 14)),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 8),
                                                  child: Text(
                                                      "${rupiah(listProduct[index].harga)}",
                                                      textAlign: TextAlign.left,
                                                      style: GoogleFonts
                                                              .kodchasan()
                                                          .copyWith(
                                                              color:
                                                                  Colors.blue,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 10)),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 0),
                                                  child: Text("/ porsi",
                                                      textAlign: TextAlign.left,
                                                      style: GoogleFonts
                                                              .kodchasan()
                                                          .copyWith(
                                                              color:
                                                                  Colors.black,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 10)),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                        flex: 1,
                                        child: Container(
                                            child: listProduct[index]
                                                        .isListed ==
                                                    false
                                                ? Icon(Icons.bookmark_border)
                                                : Icon(
                                                    Icons.bookmark,
                                                    color: Colors.blue,
                                                  )))
                                  ],
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                              ],
                            )),
                      ));
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 1.0,
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 10.0,
                ),
              ),
            ),
            Column(
              children: [
                Row(children: <Widget>[
                  Expanded(child: Divider()),
                  InkWell(
                    onTap: () {
                      bottomSheetProduct(SizeConfig.screenHight,
                          SizeConfig.screenWidth, listProductcard, context);
                    },
                    child: Icon(
                      Icons.arrow_upward,
                      color: Colors.blue,
                      size: 30,
                    ),
                  ),
                  Expanded(child: Divider()),
                ]),
                Center(
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Icon(
                          Icons.add_shopping_cart,
                          color: Colors.blue,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Text(rupiah(totalHarga),
                            style: GoogleFonts.kodchasan().copyWith(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 14)),
                      ),
                      Spacer(),
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.blue)),
                          onPressed: () {
                            doReset();
                          },
                          color: Colors.blue,
                          textColor: Colors.white,
                          child: Text("Reset",
                              style: GoogleFonts.kodchasan().copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.blue)),
                          onPressed: () {
                            if(totalHarga != 0 ){
                              dialogCharge(context,totalHarga);
                            }else{
                              showToast(context, "tidak ada yg bisa dibayar");
                            }
                          },
                          color: Colors.blue,
                          textColor: Colors.white,
                          child: Text("Charge",
                              style: GoogleFonts.kodchasan().copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18)),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void addProduct() {
    setState(() {
      listProduct.add(new ItemProduct(
          harga: 10000,
          qty: 1,
          img: "assets/img/food.jpg",
          name: "Mie bu haji",
          isListed: false,
          id: 1));
      listProduct.add(new ItemProduct(
          harga: 20000,
          qty: 1,
          img: "assets/img/food.jpg",
          name: "Mie bu haji",
          isListed: false,
          id: 2));
      listProduct.add(new ItemProduct(
          harga: 30000,
          qty: 1,
          img: "assets/img/food.jpg",
          name: "Mie bu haji",
          isListed: false,
          id: 3));
      listProduct.add(new ItemProduct(
          harga: 40000,
          qty: 1,
          img: "assets/img/food.jpg",
          name: "Mie bu haji",
          isListed: false,
          id: 4));
      listProduct.add(new ItemProduct(
          harga: 50000,
          qty: 1,
          img: "assets/img/food.jpg",
          name: "Mie bu haji",
          isListed: false,
          id: 5));
      listProduct.add(new ItemProduct(
          harga: 60000,
          qty: 1,
          img: "assets/img/food.jpg",
          name: "Mie bu haji",
          isListed: false,
          id: 6));
      listProduct.add(new ItemProduct(
          harga: 70000,
          qty: 1,
          img: "assets/img/food.jpg",
          name: "Mie bu haji",
          isListed: false,
          id: 7));
      listProduct.add(new ItemProduct(
          harga: 80000,
          qty: 1,
          img: "assets/img/food.jpg",
          name: "Mie bu haji",
          isListed: false,
          id: 8));
      listProduct.add(new ItemProduct(
          harga: 10000,
          qty: 1,
          img: "assets/img/food.jpg",
          name: "Mie bu haji",
          isListed: false,
          id: 9));
      listProduct.add(new ItemProduct(
          harga: 10000,
          qty: 1,
          img: "assets/img/food.jpg",
          name: "Mie bu haji",
          isListed: false,
          id: 10));
    });
  }

  int qtyProduct = 1;

  void onClickProduct(ItemProduct listProduct) {
    setState(() {
      listProduct.qty += 1;
      // totalHarga += listProduct.harga;
      bool isListed = false;

      for (int i = 0; i < listProductcard.length; i++) {
        if (listProductcard[i].id == listProduct.id) {
          setState(() {
            // int qty = qtyProduct + listProduct.qty;
            listProductcard[i].qty += 1;
            isListed = true;
            print(' dsadasdsa');
          });
        }
      }
      if (!isListed) {
        setState(() {
          listProductcard.add(new ModelListProduct(
            name: listProduct.name,
            qty: 1,
            id: listProduct.id,
            harga: listProduct.harga,
            img: listProduct.img,
          ));
        });
      }
    });
  }

  void doReset() {
    setState(() {
      totalQty = 0;
      totalHarga = 0;
      listProductcard.clear();
      // listProduct.clear();
    });
  }

  void bottomSheetProduct(double screenHight, double screenWidth,
      List<ModelListProduct> listProductcard, BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return Container(
          color: Colors.grey[700],
          child: Container(
            height: screenHight / 1.3,
            child: ListView.builder(
              physics: ClampingScrollPhysics(),
              itemCount: listProductcard.length,
              scrollDirection: Axis.vertical,
              itemBuilder: (context, index) {
                return Container(
                    child: InkWell(
                  onTap: () {
                    // onClickProduct(listProduct[index]);
                    // print(listProductcard.length.toString() + " dhsajl");
                  },
                  child: Card(
                      elevation: 4,
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: [
                              Flexible(
                                flex: 1,
                                child: Container(
                                  child: Image.asset(
                                    listProductcard[index].img,
                                    fit: BoxFit.cover,
                                    width: screenWidth / 4,
                                    height: 100,
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(left: 4),
                                        child: Text(
                                            "${listProductcard[index].name}",
                                            textAlign: TextAlign.left,
                                            style: GoogleFonts.kodchasan()
                                                .copyWith(
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black,
                                                    fontSize: 14)),
                                      ),
                                      SizedBox(
                                        height: 4,
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(left: 4),
                                            child: Text(
                                                "${rupiah(listProductcard[index].harga)}",
                                                textAlign: TextAlign.left,
                                                style: GoogleFonts.kodchasan()
                                                    .copyWith(
                                                        color: Colors.blue,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 10)),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 0),
                                            child: Text("/ porsi",
                                                textAlign: TextAlign.left,
                                                style: GoogleFonts.kodchasan()
                                                    .copyWith(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 10)),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Container(
                                      width: 30,
                                      decoration: BoxDecoration(
                                        color: Colors.black,
                                        shape: BoxShape.circle,
                                      ),
                                      child: Center(
                                        child: IconButton(
                                          onPressed: () {
                                            minQuantity(listProductcard[index],
                                                context);
                                          },
                                          icon: Icon(
                                            Icons.remove,
                                            color: Colors.white,
                                            size: 16,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Text('${listProductcard[index].qty}'),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Container(
                                      width: 30,
                                      decoration: BoxDecoration(
                                        color: Colors.black,
                                        shape: BoxShape.circle,
                                      ),
                                      child: Center(
                                        child: IconButton(
                                          onPressed: () {
                                            addQuantity(listProductcard[index],
                                                context);
                                          },
                                          icon: Icon(
                                            Icons.add,
                                            color: Colors.white,
                                            size: 16,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          SizedBox(
                            height: 4,
                          ),
                        ],
                      )),
                ));
              },
            ),
          ),
        );
      },
    );
  }

  void addQuantity(ModelListProduct list, BuildContext context) async {
    setState(() {
      if (list.qty > 7) {
        showToast(context, "Quantity tidak boleh lebih dari 8");
      } else {
        list.qty++;
      }
    });
    await Navigator.pop(context);
    await bottomSheetProduct(SizeConfig.screenHight, SizeConfig.screenWidth,
        listProductcard, context);
    totalHargas();
  }

  void minQuantity(ModelListProduct list, BuildContext context) async {
    setState(() {
      if (list.qty < 2) {
        list.qty = 1;
      } else {
        list.qty--;
      }
      totalHargas();
    });
    await Navigator.pop(context);
    await bottomSheetProduct(SizeConfig.screenHight, SizeConfig.screenWidth,
        listProductcard, context);
  }

  void totalHargas() {
    setState(() {
      int harga = 0;
      for (int i = 0; i < listProductcard.length; i++) {
        harga += (listProductcard[i].harga * listProductcard[i].qty);
      }
      totalHarga = harga;
      totalHargaController.text = rupiah(totalHarga.toString());
    });
  }

  void dialogCharge(BuildContext context, int totalHarga) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    successTicket(context,totalHarga),
                    SizedBox(
                      height: 10.0,
                    ),
                    FloatingActionButton(
                      backgroundColor: Colors.black,
                      child: Icon(
                        Icons.clear,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )
                  ],
                ),
              ),
            ));
  }

  Widget successTicket(BuildContext context, int totalHarga) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(16.0),
      child: Material(
        clipBehavior: Clip.antiAlias,
        elevation: 2.0,
        borderRadius: BorderRadius.circular(4.0),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              ProfileTile(
                title: "Confirmasi ",
                textColor: Colors.blue,
                subtitle: "Detail Pemesanan",
              ),
              ListTile(
                title: Padding(
                  padding: EdgeInsets.only(bottom: 10.0, top: 00.0),
                  child: new TextFormField(
                    keyboardType: TextInputType.number,
                    controller: uangController,
                    decoration: new InputDecoration(
                      labelText: 'Uang yang dibayar',
                      labelStyle: new TextStyle(color: Colors.black),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: const Color(0xFF424242)),
                      ),
                    ),
                  ),
                ),
              ),
              ListTile(
                title: Padding(
                  padding: EdgeInsets.only(bottom: 10.0, top: 00.0),
                  child: new TextFormField(
                    enabled: false,
                    controller: totalHargaController,
                    decoration: new InputDecoration(
                      labelText: 'Total Harga',
                      labelStyle: new TextStyle(color: Colors.black),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: const Color(0xFF424242)),
                      ),
                    ),
                  ),
                ),
              ),
              ListTile(
                title: Padding(
                  padding: EdgeInsets.only(bottom: 10.0, top: 00.0),
                  child: new TextFormField(
                    enabled: false,
                    controller: kembalianController,
                    decoration: new InputDecoration(
                      labelText: 'Kembalian',
                      labelStyle: new TextStyle(color: Colors.black),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: const Color(0xFF424242)),
                      ),
                    ),
                  ),
                ),
              ),
              Card(
                clipBehavior: Clip.antiAlias,
                elevation: 0.0,
                color: Colors.grey.shade300,
                child: ListTile(
                  title: Padding(
                    padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                    child: new RaisedButton(
                      child: Text('Charge'),
                      color: Colors.blue,
                      textColor: Colors.white,
                      onPressed: () {
                        if(uangController.isNullOrBlank){
                          showToast(context, "tidak boleh kosong");
                          setState(() {
                            kembalianController.text = "";
                          });
                        }else{
                          if(int.parse(uangController.text.toString()) < totalHarga){
                            showToast(context, "uang lebih kecil dari harga");
                          }else{
                            int kembali =  int.parse(uangController.text.toString()) - totalHarga;
                            kembalianController.text = rupiah(kembali.toString());
                          }
                        }
                      },
                    ),
                  ),
                ),
              ),
              Card(
                clipBehavior: Clip.antiAlias,
                elevation: 0.0,
                color: Colors.grey.shade300,
                child: ListTile(
                  title: Padding(
                    padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                    child: new RaisedButton(
                      child: Text('Print'),
                      color: Colors.blue,
                      textColor: Colors.white,
                      onPressed: () {
                        if(kembalianController.text.isNullOrBlank){
                          showToast(context, "tidak ada data yang bisa di print");
                        }else{
                          Navigator.push(
                              context, MaterialPageRoute(builder: (context) => PrintPage()));
                        }
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
